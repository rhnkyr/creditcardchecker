﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CreditCardChecker.Helpers;
using CreditCardChecker.Models;
using CreditCardValidator;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CreditCardChecker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : Controller
    {
        private readonly CardContext             _context;
        private readonly IConfiguration          _configuration;
        private readonly ILogger<CardController> _log;
        private readonly IHttpContextAccessor    _accessor;

        public CardController(CardContext context, IConfiguration configuration, ILogger<CardController> log,
            IHttpContextAccessor accessor)
        {
            _context       = context;
            _configuration = configuration;
            _log           = log;
            _accessor      = accessor;

            if (context.Cards.Count() != 0) return;

            try
            {
                //INFO: To store CC data in database have to encrypt it. The problem was if we want to check cc number against decrypted row data we have to have a identifier for that. Hence I added HolderFullName as identifier.  

                //Seed dummy data
                //visa card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "Erhan Kayar",
                    Generic.EncryptTripleDes("4111111111111111", _configuration["PassPhrase"]), CardType.VISA, 11,
                    2020);
                //visa card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "John Doe",
                    Generic.EncryptTripleDes("4012888888881881", _configuration["PassPhrase"]), CardType.VISA, 12,
                    2024);
                //master card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "Jane Doe",
                    Generic.EncryptTripleDes("5555555555554444", _configuration["PassPhrase"]), CardType.MASTERCARD, 11,
                    2027);
                //master card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "James Gosling",
                    Generic.EncryptTripleDes("5105105105105100", _configuration["PassPhrase"]), CardType.MASTERCARD, 11,
                    2029);
                //amex card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "John Appleseed",
                    Generic.EncryptTripleDes("378282246310005", _configuration["PassPhrase"]), CardType.AMEX, 10,
                    2019);
                //amex card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "Isac Newton",
                    Generic.EncryptTripleDes("371449635398431", _configuration["PassPhrase"]), CardType.AMEX, 12,
                    2020);
                //jcb card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "Albert Einstein",
                    Generic.EncryptTripleDes("3530111333300000", _configuration["PassPhrase"]), CardType.JCB, 6,
                    2028);
                //jcb card
                _context.Database.ExecuteSqlCommand("sp_InsertCard @p0,@p1,@p2,@p3,@p4", "Stephen Hawking",
                    Generic.EncryptTripleDes("3566002020360505", _configuration["PassPhrase"]), CardType.JCB, 3,
                    2026);
            }
            catch (SqlException ex)
            {
                _log.LogError(ex.Message);
            }
        }

        //GET api/card
        [HttpGet]
        public async Task<List<Card>> Get()
        {
            return await _context.Cards.ToListAsync();
        }

        //POST api/card
        [HttpPost]
        public async Task<JsonResult> CardInformation(Card card)
        {
            var ip = _accessor.HttpContext.Connection.RemoteIpAddress;

            //3rd party Credit Card Detector for server side validation

            //Check against Luhn
            if (!Luhn.CheckLuhn(card.Number))
            {
                _log.LogError("Invalid or Unknown Card : " + card.Number + " with IP : " + ip);
                return ValidationResult(false, "Invalid or Unknown Card");
            }

            //Detect card by number
            var detector = new CreditCardDetector(card.Number);

            //Check the card is valid
            if (!detector.IsValid())
            {
                _log.LogError("Unknown Card : " + card.Number + " with IP : " + ip);
                return ValidationResult(false, "Unknown Card");
            }

            //Check the provided rules
            switch (detector.Brand)
            {
                case CardIssuer.Visa when !DateTime.IsLeapYear(card.Year) && MonthAndYearCheck(card):
                case CardIssuer.MasterCard when !Generic.IsPrime(card.Year) && MonthAndYearCheck(card):
                    _log.LogError("Invalid Card : " + card.Number + " with IP : " + ip);
                    ValidationResult(false, "Invalid Card");
                    break;
            }

            //Check the DB
            //INFO: Use HolderFullName as identifier.
            try
            {
                var cardInformation = await _context.Cards.FromSql("sp_CheckCard {0}, {1}, {2}", card.HolderFullName,
                    card.Month,
                    card.Year).FirstOrDefaultAsync();

                var check = Generic.DecryptTripleDes(cardInformation.Number, _configuration["PassPhrase"]) ==
                            card.Number.Replace(" ", "");

                return ValidationResult(check, "Card has been found");
            }
            catch (NullReferenceException nex)
            {
                _log.LogError("Card does not exist: " + card.Number + " with IP : " + ip);
                return ValidationResult(false, "Does not exist");
            }
        }

        /// <summary>
        /// Json string helper
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private JsonResult ValidationResult(bool status, string message)
        {
            return Json(new {status, message});
        }

        /// <summary>
        /// Check cards month and year
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        private static bool MonthAndYearCheck(Card card)
        {
            return card.Year >= DateTime.Now.Year && card.Month > DateTime.Now.Month;
        }
    }
}
