create table dbo.Cards
(
  Id             bigint identity
    primary key,
  HolderFullName nvarchar(100),
  Number         varchar(100),
  Type           tinyint,
  Month          int not null,
  Year           int not null
  
)
go

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertCard]
    @HolderFullName varchar(100),
    @Number         varchar(100),
    @Type           tinyint,
    @Month          int,
    @Year           int
AS
  BEGIN
    SET NOCOUNT ON;
    insert into Cards(HolderFullName, Number, Type, [Month], [Year]) VALUES (@HolderFullName, @Number, @Type, @Month, @Year)
  END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_CheckCard]
    @HolderName varchar(100),
    @Month int,
    @Year int
AS
  BEGIN
    SET NOCOUNT ON;
    SELECT * From Cards WHERE HolderFullName = @HolderName AND Month = @Month AND Year = @Year
  END
GO