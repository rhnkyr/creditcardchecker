namespace CreditCardChecker.Helpers
{
    public enum CardType
    {
        VISA       = 1,
        MASTERCARD = 2,
        AMEX       = 3,
        JCB        = 4
    }
}
