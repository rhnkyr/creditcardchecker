using System;

namespace CreditCardChecker.Helpers
{
    public static class Generic
    {
        /// <summary>
        /// Prime number checker
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        public static bool IsPrime(int candidate)
        {
            int i;
            for (i = 2; i <= candidate - 1; i++)
            {
                if (candidate % i == 0)
                {
                    return false;
                }
            }

            return i == candidate;
        }
        
        private static byte[] _buffer = new byte[0];
        
        /// <summary>
        /// Encryption method for credit card
        /// </summary>
        /// <param name="plaintext"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string EncryptTripleDes(string plaintext, string key)
        {

            var des = new System.Security.Cryptography.TripleDESCryptoServiceProvider();

            var hashMd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            des.Key = hashMd5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(key));

            des.Mode = System.Security.Cryptography.CipherMode.ECB;

            var desEncrypt = des.CreateEncryptor();

            _buffer = System.Text.Encoding.ASCII.GetBytes(plaintext);

            var tripleDes = Convert.ToBase64String(desEncrypt.TransformFinalBlock(_buffer, 0, _buffer.Length));

            return tripleDes;

        }
        
        /// <summary>
        /// Decryption method for credit card
        /// </summary>
        /// <param name="base64Text"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DecryptTripleDes(string base64Text, string key)
        {

            var des = new System.Security.Cryptography.TripleDESCryptoServiceProvider();

            var hashMd5 = new System.Security.Cryptography.MD5CryptoServiceProvider();

            des.Key = hashMd5.ComputeHash(System.Text.Encoding.ASCII.GetBytes(key));

            des.Mode = System.Security.Cryptography.CipherMode.ECB;

            var desDecrypt = des.CreateDecryptor();

            _buffer = Convert.FromBase64String(base64Text);

            var decTripleDes = System.Text.Encoding.ASCII.GetString(desDecrypt.TransformFinalBlock(_buffer, 0, _buffer.Length));

            return decTripleDes;

        }
    }
}
