﻿namespace CreditCardChecker.Models
{
    public class Card
    {
        public long Id { get; set; }
        
        public string HolderFullName { get; set; }
        
        public string Number { get; set; }
        
        public byte Type { get; set; }
        
        public int Month { get; set; }
        
        public int Year { get; set; }

    }
}
