using System;
using CreditCardChecker.Helpers;
using CreditCardChecker.Models;
using Xunit;
using CreditCardValidator;

namespace CreditCardCheckerTest
{
    public class TestCards
    {
        
        
        [Fact]
        public void ValidCard()
        {
            var card = new Card {Number = "4111111111111111"};

            var result = Luhn.CheckLuhn(card.Number);
            
            Assert.True(result);
        }
        
        [Fact]
        public void InValidCard()
        {
            var card = new Card {Number = "41111111111"};

            var result = Luhn.CheckLuhn(card.Number);
            
            Assert.False(result);
        }
        
        [Fact]
        public void ValidCardByDate()
        {
            var card = new Card {Number = "4111111111111111", Month = 11, Year = 2020};

            var result = Luhn.CheckLuhn(card.Number) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;
            
            Assert.True(result);
        }
        
        [Fact]
        public void InValidCardByDate()
        {
            var card = new Card {Number = "41111111111", Month = 6, Year = 2018};

            var result = Luhn.CheckLuhn(card.Number) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;
            
            Assert.False(result);
        }
        
        [Fact]
        public void ValidVisaCard()
        {
            var card = new Card {Number = "4111111111111111", Month = 11, Year = 2020};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.Visa &&
                         DateTime.IsLeapYear(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.True(result);
        }


        [Fact]
        public void InValidVisaCard()
        {
            var card = new Card {Number = "5111111111111111", Month = 11, Year = 2020};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.Visa &&
                         DateTime.IsLeapYear(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }

        [Fact]
        public void InValidLepYearVisaCard()
        {
            var card = new Card {Number = "4111111111111111", Month = 11, Year = 2019};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.Visa &&
                         DateTime.IsLeapYear(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }

        [Fact]
        public void ValidMasterCard()
        {
            var card = new Card {Number = "5555555555554444", Month = 11, Year = 2027};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.MasterCard &&
                         Generic.IsPrime(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.True(result);
        }

        [Fact]
        public void InValidMasterPrimeCard()
        {
            var card = new Card {Number = "5555555555554444", Month = 11, Year = 2019};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.MasterCard &&
                         Generic.IsPrime(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }

        [Fact]
        public void InValidMasterCard()
        {
            var card = new Card {Number = "378282246310005", Month = 11, Year = 2022};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.MasterCard &&
                         Generic.IsPrime(card.Year) &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }
        
        
        [Fact]
        public void ValidAmexCard()
        {
            var card = new Card {Number = "378282246310005", Month = 11, Year = 2020};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.AmericanExpress &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.True(result);
        }

        [Fact]
        public void InValidAmexCard()
        {
            var card = new Card {Number = "5555555555554444", Month = 11, Year = 2019};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.AmericanExpress &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }
        
        [Fact]
        public void ValidJcbCard()
        {
            var card = new Card {Number = "3530111333300000", Month = 11, Year = 2020};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.JCB &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.True(result);
        }

        [Fact]
        public void InValidJcbCard()
        {
            var card = new Card {Number = "5555555555554444", Month = 11, Year = 2019};

            var detector = new CreditCardDetector(card.Number);
            var result = Luhn.CheckLuhn(card.Number) &&
                         detector.IsValid() &&
                         detector.Brand == CardIssuer.JCB &&
                         card.Year >= DateTime.Now.Year &&
                         card.Month > DateTime.Now.Month;

            Assert.False(result);
        }
    }
}
